/*
SQLyog Community v12.4.2 (64 bit)
MySQL - 10.1.22-MariaDB : Database - db1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db1` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db1`;

/*Table structure for table `almacen` */

DROP TABLE IF EXISTS `almacen`;

CREATE TABLE `almacen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insumo_id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_almacen_insumos1_idx` (`insumo_id`),
  CONSTRAINT `fk_almacen_insumos1` FOREIGN KEY (`insumo_id`) REFERENCES `insumos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `almacen` */

/*Table structure for table `compra_detalle` */

DROP TABLE IF EXISTS `compra_detalle`;

CREATE TABLE `compra_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compras_id` int(11) NOT NULL,
  `insumos_id` int(11) NOT NULL,
  `nombre_insumo` varchar(64) DEFAULT NULL,
  `precio_insumo` decimal(10,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_compra_detalle_compras1_idx` (`compras_id`),
  KEY `fk_compra_detalle_insumos1_idx` (`insumos_id`),
  CONSTRAINT `fk_compra_detalle_compras1` FOREIGN KEY (`compras_id`) REFERENCES `compras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_detalle_insumos1` FOREIGN KEY (`insumos_id`) REFERENCES `insumos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `compra_detalle` */

/*Table structure for table `compras` */

DROP TABLE IF EXISTS `compras`;

CREATE TABLE `compras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `sucursales_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_compras_sucursales1_idx` (`sucursales_id`),
  KEY `fk_compras_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_compras_sucursales1` FOREIGN KEY (`sucursales_id`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_compras_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `compras` */

/*Table structure for table `empleados` */

DROP TABLE IF EXISTS `empleados`;

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(10) DEFAULT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `apaterno` varchar(32) DEFAULT NULL,
  `amaterno` varchar(32) DEFAULT NULL,
  `direccion` varchar(128) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `correo` varchar(64) DEFAULT NULL,
  `sucursales_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_empleados_sucursales1_idx` (`sucursales_id`),
  CONSTRAINT `fk_empleados_sucursales1` FOREIGN KEY (`sucursales_id`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `empleados` */

insert  into `empleados`(`id`,`clave`,`nombre`,`apaterno`,`amaterno`,`direccion`,`telefono`,`correo`,`sucursales_id`,`status`) values 
(2,'YL2017','Yael','Lira ','Lizarraga','Criollos #16009 Los sauses','(669)9402116','yaellizarraga@outlook.es',1,1),
(3,'CM2017','Clarissa','Martinez','Leyva','Su direccion','(669)2342345','clarissa@ingcell.com',1,1),
(6,'PM2017','Patrick','Perez','Lopez','Carnaval #4599 Col. Centro','6699402116','juan@valadolod.com',NULL,1),
(7,'AP2017','Alejandro','Paez','Tirado','Carnaval #46546 Col. Centro','6699402116','yaellizarraga@outlook.es',NULL,1),
(8,'DD2017','Enrique','Parente','Gonzales','Su casa #17883','772623692','yael.comq@om.com',NULL,1),
(9,'AA2017','hehe','hehe','hehe','asdasd','323432','sdfsdf@dsfsd.com',NULL,1),
(10,'17','Germancito','Villa','Lizarraga','Su direccion #3232 Los laureles','6699402116','estewe@wey.com',NULL,1),
(11,'NP2017','Nancy','Padilla','Guerrero','su direccion','6699402116','yaellizarraga@outlook.es',NULL,1),
(12,'NLM2017','Nayely','Lizarraga','Madrid','Su casa','6699402116','nayeli@suco.com',NULL,1),
(13,'PEO5892017','Paco','Ronaldo','Modric','su casa','6699402116','sucorreo@correo.com',NULL,1),
(14,'RPV9322017','Ruben','Perez','Vazquez','Villas del gay','6699402116','rur@hotmail.com',NULL,1);

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

insert  into `grupo`(`id`,`nombre`,`status`) values 
(1,'Carcasas',1),
(3,'Cargadores',1),
(4,'test 4 mod',1),
(5,'test 2 mod',1),
(6,'test 3 mod',1),
(7,'final test mod',1),
(8,'final test mod',1);

/*Table structure for table `grupo_insumo` */

DROP TABLE IF EXISTS `grupo_insumo`;

CREATE TABLE `grupo_insumo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insumos_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_insumos_has_grupo_grupo1_idx` (`grupo_id`),
  KEY `fk_insumos_has_grupo_insumos1_idx` (`insumos_id`),
  CONSTRAINT `fk_insumos_has_grupo_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_insumos_has_grupo_insumos1` FOREIGN KEY (`insumos_id`) REFERENCES `insumos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grupo_insumo` */

/*Table structure for table `insumos` */

DROP TABLE IF EXISTS `insumos`;

CREATE TABLE `insumos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(20) DEFAULT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `descripcion` text,
  `img` varchar(200) DEFAULT NULL,
  `marca_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_insumos_marcas_idx` (`marca_id`),
  CONSTRAINT `fk_insumos_marcas` FOREIGN KEY (`marca_id`) REFERENCES `marcas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `insumos` */

insert  into `insumos`(`id`,`clave`,`nombre`,`descripcion`,`img`,`marca_id`,`grupo_id`,`modelo`,`precio`,`status`) values 
(2,'T2D2017','test img','sdfsd','assets/img/uploads/products/10527538_1568327206727771_441548712802226095_n.jpg',2,6,'dsfsdf',3.00,1),
(3,'O3A2017','otro','sadas','assets/img/uploads/products/28616_130693823612457_4279666_n.jpg',3,5,'asdsa',7.00,1),
(4,'D4D2017','dsfsd','sdfsd','assets/img/uploads/products/Martha 20170515_224027.jpg',4,6,'dsfds',6.00,1),
(5,'D6D2017','dfsdf','sdfsdf','assets/img/uploads/producs/1muuzw.jpg',6,4,'dsfds',2.00,1),
(6,'F1D2017','final ','dsfdssfds','assets/img/uploads/products/13239874_1437190669640224_4293938919660691179_n.jpg',1,6,'dsfsd',23.00,1),
(7,'D6D2017','dsffsd','dsfds','assets/img/uploads/producs/7tyk44.jpg',6,4,'dsfs',25.00,1),
(8,'E3E2017','ewrewrew','werew','assets/img/uploads/producs/10947236_10204580255919582_5119108507558081324_n.jpg',3,3,'erwe',32.00,1),
(9,'A3S2017','Martha','Es una bueba muchacha.','assets/img/uploads/products/FB_IMG_1494959856143.jpg',3,6,'sadsa',56.00,1);

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT NULL,
  `table` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `source_code` bigint(20) unsigned DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_logs_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_logs_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `logs` */

/*Table structure for table `marcas` */

DROP TABLE IF EXISTS `marcas`;

CREATE TABLE `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `marcas` */

insert  into `marcas`(`id`,`nombre`,`status`) values 
(1,'Mobo',1),
(2,'Sony mod',1),
(3,'Adata',1),
(4,'KingsTong',1),
(6,'final test',1);

/*Table structure for table `sucursales` */

DROP TABLE IF EXISTS `sucursales`;

CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) DEFAULT NULL,
  `direccion` varchar(128) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sucursales` */

insert  into `sucursales`(`id`,`nombre`,`direccion`,`telefono`,`status`) values 
(1,'test','test','test',1),
(2,'INGCELL Centro','Teniente Azueta Col. Centro. Mazatlan Sinaloa.','9801010',1),
(3,'INGCELL Galerias','Av. del Delfin. La marina Mazatlan','9902050',1),
(4,'test final','esta','23432423',1),
(5,'INGCELL Dubai.','Emiratos arabes unidos.','323254323',1);

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empleado_id` int(11) NOT NULL,
  `usuario` varchar(32) DEFAULT NULL,
  `contra` varchar(64) DEFAULT NULL,
  `rol` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_empleados1_idx` (`empleado_id`),
  CONSTRAINT `fk_usuarios_empleados1` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`empleado_id`,`usuario`,`contra`,`rol`,`estado`) values 
(2,2,'yaellizarraga','123',1,1),
(5,14,'test','123',1,0),
(6,3,'clarissa','123',2,1);

/*Table structure for table `venta_detalle` */

DROP TABLE IF EXISTS `venta_detalle`;

CREATE TABLE `venta_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insumos_id` int(11) NOT NULL,
  `ventas_id` int(11) NOT NULL,
  `nombre_insumo` varchar(64) DEFAULT NULL,
  `precio_insumo` decimal(10,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_venta_detalle_ventas1_idx` (`ventas_id`),
  KEY `fk_venta_detalle_insumos1_idx` (`insumos_id`),
  CONSTRAINT `fk_venta_detalle_insumos1` FOREIGN KEY (`insumos_id`) REFERENCES `insumos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_detalle_ventas1` FOREIGN KEY (`ventas_id`) REFERENCES `ventas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `venta_detalle` */

/*Table structure for table `ventas` */

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `sucursales_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ventas_sucursales1_idx` (`sucursales_id`),
  KEY `fk_ventas_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_ventas_sucursales1` FOREIGN KEY (`sucursales_id`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ventas` */

/*Table structure for table `ventas_log` */

DROP TABLE IF EXISTS `ventas_log`;

CREATE TABLE `ventas_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ventas_log` */

/* Procedure structure for procedure `Hola` */

/*!50003 DROP PROCEDURE IF EXISTS  `Hola` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `Hola`()
BEGIN
		select * from empleados;
	END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

$(document).ready(function(){
    
    var baseUrlAjax = 'http://localhost/ingcell/';
    
	$('#img').change(function(){
		$('#preview').html("");
		//$('#input').html("");
		var img = document.getElementById('img').files;
		var navegador = window.URL || window.webkitURL;
		var objeto_url = navegador.createObjectURL(img[0]);
		$('#preview').append("<img class='img-thumbnail' src="+objeto_url+" width='100%'>");
		//$('#input').append('<button type="button" class="btn btn-primary" onclick="addImagenInsumo()">Establecer imagen</button>');
	});

	$('#editImgInput').change(function(){
		$('#preview').html("");
		//$('#input').html("");
		var img = document.getElementById('editImgInput').files;
		var navegador = window.URL || window.webkitURL;
		var objeto_url = navegador.createObjectURL(img[0]);
		$('#preview').append("<img class='img-thumbnail' src="+objeto_url+" width='100%'>");
		//$('#input').append('<button type="button" class="btn btn-primary" onclick="addImagenInsumo()">Establecer imagen</button>');
	});
    
    $('#insumoCate').change(function(){
        $('#tablaUsuariosVentas tbody tr').each(function(){
            $(this).remove();
        });
        var idgrupo = $('#insumoCate option:selected').val();
        $.ajax({
            type:'POST',
            url:baseUrlAjax+'ventas/getProducts',
            data:{"grupo":idgrupo},
            success:function(res){
                //console.log(res);
                if(res.resp=='error'){
                    $('#message').html("");
                    $('#message').append("No hay productos de este tipo en stock");
                        $('#dialog').dialog({
                            modal:false,
                            buttons:{
                                Ok: function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                   }
                else{
                    for(var x=0;x<res.productos.length;x++){
                    $('#tablaUsuariosVentas tbody').append('<tr>'+
                    '<td>'+res.productos[x].clave+'</td>'+
                    '<td>'+res.productos[x].nombre+'</td>'+
                    '<td>'+res.productos[x].descripcion+'</td>'+
                    '<td>'+res.productos[x].marca+'</td>'+
                    '<td>'+res.productos[x].precio+'</td>'+
                    '<td><span class="glyphicon glyphicon-plus" aria-hidden="true" '+ 'onclick="addToSale('+res.productos[x].id+')" style="cursor: pointer !important;"></span></td></tr>');
                  }
                }
            },
            dataType:'json'
        });
    });
    
    
});

//Variables globales
var baseUrl = 'http://localhost/ingcell/';
var cantidad = 0;

function getUser(id){
	$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/getUser',
		data:{"iduser":id},
		success:function(response){
			console.log(response);
			$('#iduser').val(response.id);
			$('#empleados').val(response.empleado_id);
			$('#user').val(response.usuario);
			$('#pass').val(response.contra);
			$('#rol').val(response.rol);
			$('#estado').val(response.estado);
		},
		error:function(){
			$('#message').html("");
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
	$('#myModal').modal('show');
}

function addUser(){
	if($('#user').val()==""){
		$('#message').html("");
		$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Capture el usuario!');
		$('#dialog').dialog({
			modal:true,
			buttons:{
				Ok:function(){
					$(this).dialog("close");
				}
			}
		});
	}else if($('#pass').val()==""){
		$('#message').html("");
		$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Capture la contraseña');
		$('#dialog').dialog({
			modal:true,
			buttons:{
				Ok:function(){
					$(this).dialog("close");
				}
			}
		});
	}else{
		if($('#rol option:selected').val()==0){
		$('#message').html("");
		$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Elija un rol por favor.');
		$('#dialog').dialog({
			modal:true,
			buttons:{
				Ok:function(){
					$(this).dialog("close");
				}
			}
		});
	}else if($('#empleados').val()==0){
		$('#message').html("");
		$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Escoja un empleado por favor');
		$('#dialog').dialog({
			modal:true,
			buttons:{
				Ok:function(){
					$(this).dialog("close");
				}
			}
		});
	}else{
		$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/addUser',
		data:{
			"idempleado":$('#empleados option:selected').val(),
			"user":$('#user').val(),
			"pass":$('#pass').val(),
			"rol":$('#rol option:selected').val()
		},
		success:function(response){
			if(response.res=='ok'){
				$('#user').val("");
			    $('#pass').val("");
			    $('#empleados').val(0);
			    $('#rol').val(0);
			    $('#message').html("");
				$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Usuario Agregado con exito!');//Establecemos el mensaje del dialog
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							window.location.href = baseUrl+'usuarios/listado';
						}
					}
				});
			}else{
				$('#message').html("");
				$('#message').append("Error al agregar el Usuario");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
			}
		},
		error:function(){
			$('#message').html("");
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
	}
	}
}

function editUsuario(){
	$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/actualizarUsuario',
		data:{
				"iduser":$('#iduser').val(),
			    "idempleado":$('#empleados option:selected').val(),
				"user":$('#user').val(),
				"pass":$('#pass').val(),
				"rol":$('#rol option:selected').val(),
				"estado":$('#estado option:selected').val()
		},
		success:function(response){
			if(response.res=='ok'){
				$('#iduser').val("");
				$('#empleados').val(0);
				$('#user').val("");
				$('#pass').val("");
				$('#rol').val(0);
				$('#estado').val(3);
				$('#myModal').modal("hide");
				$('#message').html("");
				$('#message').append("Cambios guardados!");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							window.location.href = baseUrl+'usuarios/listado';
						}
					}
				});
			}
		},
		error:function(){
				$('#dialog').html("");
			    $('#dialog').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok:function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function deleteUser(id){
	$('#message').html("");
	$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>¿Seguro que desea eliminar?');
	$('#dialog').dialog({
		modal:true,
		width:400,
		buttons:{
			"Eliminar":function(){
				$.ajax({type:'POST',url:baseUrl+'usuarios/eliminarUsuario',data:{"id":id},success:function(res){
					if(res.res=='ok'){
						   window.location.href = baseUrl+'usuarios/listado';
					 }else{

					 }
				},error:function(){},dataType:'json'});
			},
			Cancel:function(){
				$("#dialog").dialog("close");
			}
		}
	});
}

function getEmpleado(id){
	$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/getEmpleado',
		data:{"idempleado":id},
		success:function(response){
			if(response.nombre != undefined){
				$('#idemp').val(response.id);
				$('#nombre').val(response.nombre);
			    $('#apa').val(response.apaterno);
			    $('#ama').val(response.amaterno);
			    $('#dir').val(response.direccion);
			    $('#tel').val(response.telefono);
			    $('#mail').val(response.correo);
			}else{
				$('#message').html("");
				$('#message').append("Ocurrio un error al traer la informacion");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
			}
		},
		error:function(){
			$('#message').html("");
			$('#message').append("Error interno del servidor");
			$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
	$('#nombre').val();
    $('#apa').val();
	$('#ama').val();
	$('#dir').val();
	$('#tel').val();
	$('#mail').val();
	$('#myModal').modal('show');
}

function getInsumo(id){
	$('#myModal').modal('show');
}

function getVenta(id){
	$('#myModal').modal('show');
}

function addEmpleados(){
	if($('.obligatorio').val()==""){
		$('#message').html("");
		$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span> Complete todos los campos por favor');//Establecemos el mensaje del dialog
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
	}else{
		$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/addEmpleado',
		data:{
			"nombre":$('#nombre').val(),
			"apa":$('#apa').val(),
			"ama":$('#ama').val(),
			"dir":$('#dir').val(),
			"tel":$('#tel').val(),
			"mail":$('#mail').val()
		},
		success:function(response){
			if(response.res=='ok'){
				$('#nombre').val("");
			    $('#apa').val("");
			    $('#ama').val("");
			    $('#dir').val("");
			    $('#tel').val("");
			    $('#mail').val("");
			    $('#message').html("");
				$('#message').html("Empleado guardado cone exito!");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							window.location.href = baseUrl+'usuarios/empleado_listado';
						}
					}
				});
			}else{
				$('#message').append("Error al agregar el Empleado");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
			}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
            alert("httpreq: "+XMLHttpRequest.responseText);
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
	}
}

function editEmpleado(){
	$.ajax({
		type:'POST',
		url:baseUrl+'usuarios/actualizarEmpleado',
		data:{
			"idempleado":$('#idemp').val(),
			"nombre":$('#nombre').val(),
			"apa":$('#apa').val(),
			"ama":$('#ama').val(),
			"dir":$('#dir').val(),
			"tel":$('#tel').val(),
			"mail":$('#mail').val()
		},
		success:function(response){
			if(response.res=='ok'){
				$('#nombre').val("");
			    $('#apa').val("");
			    $('#ama').val("");
			    $('#dir').val("");
			    $('#tel').val("");
			    $('#mail').val("");
			    $('#myModal').modal("hide");
			    $('#message').html("");
				$('#message').html("Cambios guardados!");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							window.location.href = baseUrl+'usuarios/empleado_listado';
						}
					}
				});
				reaload('empleados');
			}else{
				$('#message').html("");
				$('#message').append("Error al agregar el Empleado");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
			}
		},
		error:function(){
			$('#message').html("");
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function deleteEmpleado(id){
	$('#message').html("");
	$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>¿Seguro que desea eliminar?');
	$('#dialog').dialog({
		modal:true,
		width:400,
		buttons:{
			"Eliminar":function(){
				$.ajax({type:'POST',url:baseUrl+'usuarios/eliminarEmpleado',data:{"id":id},success:function(res){
					if(res.res=='ok'){
							window.location.href = baseUrl+'usuarios/empleado_listado';
					}else{

					}
				},error:function(){},dataType:'json'});
			},
			Cancel:function(){
				$("#dialog").dialog("close");
			}
		}
	});
}

function deleteInsumo(){
	if(confirm('¿Seguro que desea eliminar?')){

	}else{
		return false;
	}
}

function deleteVenta(){
	if(confirm('¿Seguro que desea eliminar?')){

	}else{
		return false;
	}
}

/*

	Bloque JavaScript perteneciente al modulo de Insumos

*/

function addImagenInsumo(ins){
	var img = new FormData($('#imgInsumo')[0]);
	$.ajax({
		type:'POST',
		url:baseUrl+'insumos/addImgInsumo?insumo='+ins,
		data:img,
		contentType:false,
		processData:false,
		success:function(response){
			console.log(response);
		},
		error:function(){}
	});
}

function editImgInsumo(idinsumo, imagen){
	$.ajax({
		type:'POST',
		url:baseUrl+'insumos/editImgInsumo?insumo='+idinsumo,//mandamos por get en la url el id del insumo
		data:imagen,
		contentType:false,
		processData:false,
		success:function(response){
			console.log(response);
		},
		error:function(){}
	});
}


function addInsumo(){
    $.ajax({
        type:'POST',
        url:baseUrl+'insumos/addInsumo',
        data:{
            "nombre":$('#nombre').val(),
            "descripcion":$('#descripcion').val(),
            "grupo_id":$('#grupo_id option:selected').val(),
            "marca_id":$('#marca_id option:selected').val(),
            "modelo":$('#modelo').val(),
            "precio":$('#precio').val()
        },
        success:function(response){
        	//console.log(response.id_insumo);
            if(response.res=='ok'){
            	    addImagenInsumo(response.id_insumo);
                    $('#nombre').val("");
                    $('#descripcion').val("");
                    $('#grupo_id').val("");
                    $('#marca_id').val("");
                    $('#modelo').val("");
                    $('#precio').val("");
                    $('#message').html("");
                $('#message').append("Insumo agregado con exito!");
                $('#dialog').dialog({
                    modal:false,
                    buttons:{
                        ok: function(){
                            window.location.href = baseUrl+'insumos/listado';
                        }
                    }
                });
            }else{
            	$('#message').html("");
                $('#message').append("Error al agregar insumo");
                $('#dialog').dialog({
                    modal:false,
                    buttons:{
                        ok: function(){
                            $(this).dialog('#close');
                        }
                    }
                });
            }
        },
        error:function(){
        	$('#message').html("");
            $('#message').append("Error interno del servidor");
            $('#dialog').dialog({
               modal:false,
                buttons:{
                    ok:function(){
                        $(this).dialog("close");
                    }
                }
            });
        },
        dataType:'json'
    });
}
    function getInsumo(id){

        $.ajax({
            type:'POST',
            url: baseUrl+'insumos/getInsumo',
            data:{"id_insumo":id},
            success:function(response){

                if(response.nombre != undefined){
                    $('#id_insumo').val(response.id);
                    $('#editImgInsumo').attr('src', baseUrl+response.img);
                    $('#nombre').val(response.nombre);
                    $('#descripcion').val(response.descripcion);
                    $('#marca_id').val(response.marca_id);
                    $('#grupo_id').val(response.grupo_id);
                    $('#modelo').val(response.modelo);
                    $('#precio').val(response.precio);
                }else{
                    $('#message').append("Ocurrio un error al traer informacion");
                    $('#dialog').dialog({
                        modal:false,
                        buttons:{
                            ok:function(){
                                $(this).dialog("close")
                            }
                        }
                    });
                }
            },
            error:function(){
			$('#message').append("Error interno del servidor");
			$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
            dataType:'json'
        });
        $('#nombre').val();
        $('#descripcion').val();
        $('#grupo_id').val();
        $('#marca_id').val();
        $('#modelo').val();
        $('#precio').val();
        $('#myModal').modal('show');
    }

    function editInsumo(){
    	if($('#editImgInput').get(0).files.length != 0){//Si al editar hay imagen nueva seleccionada
    		var newImg = new FormData($('#editImgForm')[0]);//Si la hay la preparamos para enviar
    		editImgInsumo($('#id_insumo').val(), newImg);
    	}
        $.ajax({
            type:'POST',
            url:baseUrl+'insumos/editInsumo',
            data:{
                "id_insumo":$('#id_insumo').val(),
                "nombre":$('#nombre').val(),
                "descripcion":$('#descripcion').val(),
                "grupo_id":$('#grupo_id').val(),
                "marca_id":$('#marca_id').val(),
                "modelo":$('#modelo').val(),
                "precio":$('#precio').val()
            },
            success:function(response){
            	console.log(response.imagen);
                if(response.res=='ok'){
                    $('#nombre').val("");
                        $('#descripcion').val("");
                        $('#grupo_id').val("");
                        $('#marca_id').val("");
                        $('#modelo').val("");
                        $('#precio').val("");
                    $('#myModal').modal("hide");
                    $('#message').append("Cambios guardados");
                    $('#dialog').dialog({
                       modal:false,
                        buttons:{
                            ok: function(){
                                window.location.href=baseUrl+'insumos/listado';
                            }
                        }
                    });
                }else{
                    $('#message').append("Error al agregar el Insumo");
                    $('#dialog').dialog({
                        modal:false,
                        buttons:{
                            ok: function(){
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            },
            error:function(){
                $('#message').append("Error interno del servidor");
                $('#dialog').dialog({
                    modal:false,
                    buttons:{
                        ok:function(){
                            $(this).dialog("close");
                        }
                    }
                });
            },
            dataType:"json"
        });
    }

function deleteInsumo(id){
    $('#message').html("");
	$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>¿Seguro que desea eliminar?');
	$('#dialog').dialog({
		modal:true,
		width:400,
		buttons:{
			"Eliminar":function(){
				$.ajax({
                    type:'POST',
                    url:baseUrl+'insumos/eliminarInsumo',
                    data:{"id":id},
                    success:function(res){
					if(res.res=='ok'){
							window.location.href = baseUrl+'insumos/listado';
					}else{

					}
				},error:function(){},dataType:'json'});
			},
			Cancel:function(){
				$("#dialog").dialog("close");
			}
		}
	});
}



/*
	Bloque de codigo JavaScript perteneciente al modulo de configuracion 23/05/2017 Yael Lizarraga
*/

 function saveGroup(){
	$.ajax({
		type:'POST',
		url:baseUrl+'config/saveGroup',
		data:{"item":$('#grupo').val()},
		success:function(response){
			if(response.res=='ok'){
				$('#message').html("");
				$('#message').append('Elemento agregado con exito!');
				$('#dialog').dialog({
					modal:false,
					buttons:{Ok:function(){window.location.href = baseUrl+'config/gestionar?active1=true';}}
				});
			}else{
				$('#message').html("");
				$('#message').append('Ha ocurrido un error al agregar el elemento!');
				$('#dialog').dialog({modal:false,buttons:{Ok:function(){$(this).dialog("close");}}});
			}
		},
		error:function(){
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function saveMarca(){
	$.ajax({
		type:'POST',
		url:baseUrl+'config/saveMarca',
		data:{"item":$('#marca').val()},
		success:function(response){
			if(response.res=='ok'){
				$('#message').html("");
				$('#message').append('Elemento agregado con exito!');
				$('#dialog').dialog({
					modal:false,
					buttons:{Ok:function(){window.location.href = baseUrl+'config/gestionar?active2=true';}}
				});
			}else{
				$('#message').html("");
				$('#message').append('Ha ocurrido un error al agregar el elemento!');
				$('#dialog').dialog({modal:false,buttons:{Ok:function(){$(this).dialog("close");}}});
			}
		},
		error:function(){
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function saveSucursal(){
	$.ajax({
		type:'POST',
		url:baseUrl+'config/saveSucursal',
		data:{"suc":$('#suc').val(), "direccion":$('#direccion').val(), "telefono":$('#telefono').val()},
		success:function(response){
			if(response.res=='ok'){
				$('#message').html("");
				$('#message').append('Elemento agregado con exito!');
				$('#dialog').dialog({
					modal:false,
					buttons:{Ok:function(){window.location.href = baseUrl+'config/gestionar?active3=true';}}
				});
			}else{
				$('#message').html("");
				$('#message').append('Ha ocurrido un error al agregar el elemento!');
				$('#dialog').dialog({modal:false,buttons:{Ok:function(){$(this).dialog("close");}}});
			}
		},
		error:function(){
		 	$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function cancelarEdit(boton){
	if(boton==1){
		$('#botonGrupo').html("");
		$('#idgrupo').val("");
		$('#grupo').val("");
		$('#botonGrupo').append('<button type="button" class="btn btn-primary" onclick="saveGroup()">Guardar</button>');
	}else{
		$('#botonMarca').html("");
		$('#idmarca').val("");
		$('#marca').val("");
		$('#botonMarca').append('<button type="button" class="btn btn-primary" onclick="saveMarca()">Guardar</button>');
	}
}


function getElement(idelement, tabla){
	$.ajax({
		type:'POST',
		url:baseUrl+'config/getElement',
		data:{"idelement":idelement , "table":tabla},
		success:function(response){
			if(response.inputText == 'grupo'){
			 $('#idgrupo').val(response.element.id);
			 $('#grupo').val(response.element.nombre);
			 $('#botonGrupo').html("");
			 $('#botonGrupo').append('<button type="button" class="btn btn-primary" onclick="editElement('+response.element.id+', 1)">Editar Elemento</button>');
			 $('#botonGrupo').append('<button type="button" style="margin-left:1%;" class="btn btn-danger" onclick="cancelarEdit(1)">Cancelar edicion</button>');
		    }else{
		     $('#idmarca').val(response.element.id);
		     $('#marca').val(response.element.nombre);
		     $('#botonMarca').html("");
		     $('#botonMarca').append('<button type="button" class="btn btn-primary" onclick="editElement('+response.element.id+', 2)">Editar Elemento</button>');
		     $('#botonMarca').append('<button type="button" style="margin-left:1%;" class="btn btn-danger" onclick="cancelarEdit(2)">Cancelar edicion</button>');
		    }
		},
		error:function(){
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});
}

function editElement(idelement, table){
	if(table==1){
		var tabla = 'grupo';
		var name = $('#grupo').val();
		var tab = 'active1';
	}else{
		var tabla = 'marcas';
		var name = $('#marca').val();
		var tab = 'active2';
	}

	$.ajax({
		type:'POST',
		url:baseUrl+'config/editElemento',
		data:{"idelemento":idelement, "table":tabla, "nombre": name},
		success:function(response){
			if(response.res == 'ok'){
				$('#message').html("");
				$('#message').append('Elemento editado con exito!');
				$('#dialog').dialog({
					modal:false,
					buttons:{Ok:function(){window.location.href = baseUrl+'config/gestionar?'+tab+'=true';}}
				});
			}else{
				$('#message').html("");
				$('#message').append('Ha ocurrido un error al editar el elemento!');
				$('#dialog').dialog({modal:false,buttons:{Ok:function(){$(this).dialog("close");}}});
			}
		},
		error:function(){
			$('#message').append("Error interno del servidor");
				$('#dialog').dialog({
					modal:false,
					buttons:{
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
		},
		dataType:'json'
	});

}

function deleteElement(id, table){

	if(table==1){
		var tab = 'active1';
		var tabla = 'grupo';
	}else{
		var tab = 'active2';
		var tabla = 'marcas';
	}

	$('#message').html("");
	$('#message').append('<span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>¿Seguro que desea eliminar?');
	$('#dialog').dialog({
		modal:true,
		width:400,
		buttons:{
			"Eliminar":function(){
				$.ajax({type:'POST',url:baseUrl+'config/deleteElement',data:{"id":id, "table":tabla},success:function(res){
					if(res.res=='ok'){
							window.location.href = baseUrl+'config/gestionar?'+tab+'=true';
					}else{

					}
				},error:function(){},dataType:'json'});
			},
			Cancel:function(){
				$("#dialog").dialog("close");
			}
		}
	});
}

//Funciones de ventas

function editCuantity(idproduct,action){
    if(action==1){
          cantidad++;
       }else{
           cantidad--;
           $.ajax({
               type:'POST',
               url:baseUrl+'ventas/resetStock',
               data:{"idproducto":idproduct},
               success:function(res){
                   if(res.message=='error'){
                        $('#message').html("");
                        $('#message').append("Error de servidor");
                        $('#dialog').dialog({
                            modal:false,
                            buttons:{
                                Ok: function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                      }
               },
               dataType:'json'
           });
       }
}

function addToSale(idproduct){
        //Verificar la existencia en stock
        $.ajax({
            type:'POST',
            url:baseUrl+'ventas/validarExistencia',
            data:{"idproducto":idproduct},
            success:function(res){
                if(res.message == 'exist'){
                        editCuantity(idproduct,1);
                $.ajax({
                    type:'POST',
                    url:baseUrl+'ventas/getProduct',
                    data:{"idproducto":idproduct},
                    success:function(res){
                        
                                console.log(res);
                                $('#tablaUsuarios4 tbody').html("");
                                $('#tablaUsuarios4 tbody').append('<tr><td>'+res.message.clave+'</td>'+
                                '<td>'+res.message.nombre+'</td>'+
                                '<td>'+res.message.descripcion+'</td>'+
                                '<td>'+res.message.marca+'</td>'+
                                '<td>'+cantidad+'</td>'+
                                '<td>'+res.message.precio+'</td>'+
                                '<td><span class="glyphicon glyphicon-minus" aria-hidden="true" '+ 'onclick="editCuantity('+res.message.id+', 0)" style="cursor: pointer !important;"></span></td></tr>');

                           

                    },
                    dataType:'json'
                });
                   }else{
                       $('#message').html("");
                        $('#message').append("Stock agotado para este producto");
                        $('#dialog').dialog({
                            modal:false,
                            buttons:{
                                Ok: function(){
                                    $(this).dialog("close");
                                }
                            }
                        });
                   }
            },
            dataType:'json'
            
        });
       
}

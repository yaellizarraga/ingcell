<?php

class UsuariosModel extends CI_Model{

	function getEmpleados(){
		return $this->db->get('empleados')->result();
	}

	function addEmpleado($data){
		if($this->db->insert('empleados', $data)){
			return true;
		}else{
			return false;
		}
	}

	function editEmpleado($id, $data){
		$this->db->where('id', $id);
		if($this->db->update('empleados', $data))
			return true;
			else
			return false;
		}

	function getUsuarios(){
		return $this->db->get('usuarios')->result();
	}


	function addUser($userData){
		if($this->db->insert('usuarios', $userData)){
			return true;
		}else{
			return false;
		}
	}

	function editUsuario($idUser, $userData){
		$this->db->where('id', $idUser);
		if($this->db->update('usuarios', $userData))
			return true;
			else
			return false;
	}
		

}
<?php

class ventasModel extends CI_Model{
    
    
    function getGrupos(){
        return $this->db->get('grupo')->result();
            
    }
    
    function getProducts($idgrupo){
        $this->db->where('grupo_id',$idgrupo);
        return $this->db->get('insumos')->result();
    }
    
}
<?php

class InsumosModel extends CI_Model{

	function getGrupo(){
        return $this->db->get('grupo')->result();
    }
    function getMarca(){
        return $this->db->get('marcas')->result();
    }
	
		
function addInsumo($insumo){
    if($this->db->insert('insumos', $insumo)){
        return true;
    }else{
        return false;
    }
}
    function getInsumos(){
       $this->db->select('insumos.id,insumos.clave,insumos.nombre,marcas.nombre as nom,insumos.precio');
        $this->db->from('insumos');
        $this->db->join('marcas','marcas.id = insumos.marca_id');
        $this->db->where('insumos.status',1);
        $query= $this->db->get();
        return $query->result();
    }
    
    function editInsumo($id_insumo, $data){
        $this->db->where('id', $id_insumo);
        if($this->db->update('insumos',$data))
            return true;
        else
            return false;
    }
   
}
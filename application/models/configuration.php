<?php 

class Configuration extends CI_Model{


	public function getGrupos(){
		return $this->db->get('grupo')->result();
	}

	public function getMarcas(){
		return $this->db->get('marcas')->result();
	}

	public function getSucursales(){
		return $this->db->get('sucursales')->result();
	}

	public function saveGroup($data){
		if($this->db->insert('grupo', $data)){
			return true;
		}else{
			return false;
		}
	}

	public function saveMarca($data){
			if($this->db->insert('marcas', $data)){
				return true;
			}else{
				return false;
			}
	}

	public function saveSucursal($data){
		if($this->db->insert('sucursales', $data)){
			return true;
		}else{
			return false;
		}
	}

}
<?php

class Usuarios extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('usuariosModel','modelo');
	}

	function getTable(){
		$tabla = $this->input->post('table');
		return $this->db->get($tabla)->result();
	}

	function crear(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['empleados'] = $this->modelo->getEmpleados();
		$data['vista'] = 'usuarios/crear';
		$this->load->view('index', $data);
	}

	function listado(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['table'] = true;
		$data['empleados'] = $this->modelo->getEmpleados();
		$data['usuarios'] = $this->modelo->getUsuarios();
		$data['vista'] = 'usuarios/listado';
		$this->load->view('index', $data);
	}

	function getEmpleado(){
		$id = $this->input->post('idempleado');
		$this->db->where('id', $id);
		echo json_encode($this->db->get('empleados')->row());
	}

	function empleado(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['vista'] = 'usuarios/empleados';
		$this->load->view('index', $data);
	}

	function empleado_listado(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['table'] = true;
		$data['empleados'] = $this->modelo->getEmpleados();
		$data['vista'] = 'usuarios/listadoEmpleados';
		$this->load->view('index', $data);
	}

	function addEmpleado(){
		$fecha = getdate();
		//Generacion de la clave iniciales del nombre y apellidos, seguido de un numero aleatorio de 3 digitos entre 100 y 999 seguido del año actual
		$clave = strtoupper(substr($this->input->post('nombre'), 0, 1)).strtoupper(substr($this->input->post('apa'), 0, 1)).strtoupper(substr($this->input->post('ama'), 0, 1)).rand(100,999).$fecha['year'];
		$empleadoData = array(
				'clave'=>$clave,
				'nombre'=>$this->input->post('nombre'),
				'apaterno'=>$this->input->post('apa'),
				'amaterno'=>$this->input->post('ama'),
				'direccion'=>$this->input->post('dir'),
				'telefono'=>$this->input->post('tel'),
				'correo'=>$this->input->post('mail')
			);
		if($this->modelo->addEmpleado($empleadoData)){echo json_encode(array('res'=>'ok'));}else{ echo json_encode(array('res'=>'error'));}
	}

	function actualizarEmpleado(){
		$id = $this->input->post('idempleado');
		$empleadoData = array(
				'nombre'=>$this->input->post('nombre'),
				'apaterno'=>$this->input->post('apa'),
				'amaterno'=>$this->input->post('ama'),
				'direccion'=>$this->input->post('dir'),
				'telefono'=>$this->input->post('tel'),
				'correo'=>$this->input->post('mail')
			);
		if($this->modelo->editEmpleado($id, $empleadoData)){echo json_encode(array('res'=>'ok'));}else{ echo json_encode(array('res'=>'error'));}
	}

	function eliminarEmpleado(){
		$id = $this->input->post('id');
		$this->db->trans_start();
		$this->db->where('empleado_id', $id);
		$this->db->delete('usuarios');
		$this->db->where('id', $id);
		$this->db->delete('empleados');
		$this->db->trans_complete();

		if($this->db->trans_status()===false)
			echo json_encode(array('res'=>'error'));
		else
			echo json_encode(array('res'=>'ok'));
	}

	function getUser(){
		$this->db->where('id', $this->input->post('iduser'));
		echo json_encode($this->db->get('usuarios')->row());
	}

	function addUser(){
		$userData = array(
				'empleado_id'=>$this->input->post('idempleado'),
				'usuario'=>$this->input->post('user'),
				'contra'=>$this->input->post('pass'),
				'rol'=>$this->input->post('rol')
			);

		if($this->modelo->addUser($userData)){echo json_encode(array('res'=>'ok'));}else{echo json_encode(array('res'=>'error'));}
	}

	function actualizarUsuario(){

		$iduser = $this->input->post('iduser');
		$usuarioData = array(
				'empleado_id'=>$this->input->post('idempleado'),
				'usuario'=>$this->input->post('user'),
				'contra'=>$this->input->post('pass'),
				'rol'=>$this->input->post('rol'),
				'estado'=>$this->input->post('estado')
			);
		if($this->modelo->editUsuario($iduser, $usuarioData)){echo json_encode(array('res'=>'ok'));}else{ echo json_encode(array('res'=>'error'));}
	}

	function eliminarUsuario(){
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		if($this->db->delete('usuarios')){echo json_encode(array('res'=>'ok'));}else{echo json_encode(array('res'=>'error'));}
	}

}
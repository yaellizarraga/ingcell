<?php 

class Config extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('configuration', 'menni');
	}

	public function gestionar(){
		$data['ventas'] = true;
		$data['stock'] = true;
		$data['usuarios'] = true;
		$data['insumos'] = true;
		$data['config'] = true;
		$data['table'] = true;
		$data['table2'] = true;
		$data['table3'] = true;
		$data['vista'] = 'config/config';
		$data['grupos'] = $this->menni->getGrupos();
		$data['marcas'] = $this->menni->getMarcas();
		$data['sucursales'] = $this->menni->getSucursales();
		$this->load->view('index', $data);
	}

	public function saveGroup(){
		$data = array('nombre'=>$this->input->post('item'));

		if($this->menni->saveGroup($data)==true){
			echo json_encode(array('res'=>'ok'));
		}else{
			echo json_encode(array('res'=>'error'));
		}
	}

	public function saveMarca(){
		$data = array('nombre'=>$this->input->post('item'));

		if($this->menni->saveMarca($data)==true){
			echo json_encode(array('res'=>'ok'));
		}else{
			echo json_encode(array('res'=>'error'));
		}
	}

	public function saveSucursal(){
		$data = array(
				'nombre'=>$this->input->post('suc'),
				'direccion'=>$this->input->post('direccion'),
				'telefono'=>$this->input->post('telefono')
			);

		if($this->menni->saveSucursal($data)==true){
			echo json_encode(array('res'=>'ok'));
		}else{
			echo json_encode(array('res'=>'error'));
		}
	}

	public function getElement(){
		$id = $this->input->post('idelement');
		$table = $this->input->post('table');
		if($table == 'grupo'){
			$this->db->where('id', $id);
		    echo json_encode(array('element'=>$this->db->get($table)->row(), 'inputText'=>'grupo'));
		}else{
			$this->db->where('id', $id);
		    echo json_encode(array('element'=>$this->db->get($table)->row(), 'inputText'=>'marca'));
		}
	}

	public function editElemento(){
		$this->db->where('id', $this->input->post('idelemento'));
		$this->db->set('nombre', $this->input->post('nombre'));
		if($this->db->update($this->input->post('table')))
			echo json_encode(array('res'=>'ok'));
		else
			echo json_encode(array('res'=>'error'));
	}

	public function deleteElement(){
		$this->db->where('id', $this->input->post('id'));
		if($this->db->delete($this->input->post('table')))
			echo json_encode(array('res'=>'ok'));
		else
			echo json_encode(array('res'=>'error'));
	}

}
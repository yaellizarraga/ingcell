<?php

class Almacen extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('login');
	}

	function home(){
		if($this->session->userdata('sesion')==true){
			/*Trae los 6 productos mas vendidos desde el inicio de operacion del sistema hasta la fecha*/
			$barChartInfo = $this->db->query('SELECT insumos.`nombre` AS producto, SUM(venta_detalle.`cantidad`) AS vendidos FROM venta_detalle INNER JOIN insumos ON insumos.`id` = venta_detalle.`insumos_id` INNER JOIN ventas ON ventas.`id` = venta_detalle.`ventas_id` GROUP BY producto LIMIT 6')->result();
			/*Trael los 5 productos con menos cantidad en el stock*/
			$donutChartInfo = $this->db->query('SELECT insumos.`nombre` AS producto, insumos.`img` AS img, almacen.`cantidad` AS cantidad FROM insumos INNER JOIN almacen ON almacen.`insumo_id` = insumos.`id` WHERE cantidad <= 10 LIMIT 5')->result();
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
				$data['barChart'] = $barChartInfo;
				$data['donutChart'] = $donutChartInfo;
				$data['vista'] = 'home';
			}else if($this->session->userdata('rol')==2){
				$data['barChart'] = $barChartInfo;
				$data['donutChart'] = $donutChartInfo;
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['vista'] = 'ventas/crear';
			}
		}
		$data['home'] = true;
		$this->load->view('index', $data);
	}

	function stock(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['table'] = true;
		$data['vista'] = 'almacen/almacen';
		$this->load->view('index', $data);
	}


	function login(){

		$user = $this->input->post('user');
		$pass = $this->input->post('password');
		$this->db->where('usuario', $user);
		$this->db->where('contra', $pass);
		$this->db->where('estado', 1);
		$res = $this->db->get('usuarios')->row();

		if(!$res){
			echo json_encode(array('res'=>'00')); //Codigo 00 = Password o contraseña incorrecta
		}else{
			$userData = array(
					'sesion'=>true,//Si hay sesion
					'user'=>$res->usuario,//Nombre de usuario
					'rol'=>$res->rol//Rol del usuario
				);
			$this->session->set_userdata($userData);
			echo json_encode(array('res'=>'01')); //Codigo 01 = Datos correctos
		}

	}

	function logout(){
		$this->session->sess_destroy();
		header('location:'.base_url());
	}

	function rol(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
	}

	function getBarChart(){
		$info = $this->db->query('SELECT insumos.`nombre` AS producto, COUNT(insumos_id) AS vendidos, ventas.`fecha` AS periodo FROM venta_detalle INNER JOIN insumos ON insumos.`id` = venta_detalle.`insumos_id` INNER JOIN ventas ON ventas.`id` = venta_detalle.`ventas_id` WHERE insumos_id = 2 AND fecha >= 17-06-15')->result();
		if($info!=false){
			echo json_encode(array('elements'=>$info, 'res'=>'ok'));
		}else{
			echo json_encode(array('elements'=>null, 'res'=>'error'));
		}
	}

}

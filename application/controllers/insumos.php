<?php 

date_default_timezone_set('America/Mazatlan');

class Insumos extends CI_Controller{

	function __construct(){
		parent::__construct();
        $this->load->model('insumosModel');
	}

	function crear(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
        $data['grupo']= $this->insumosModel->getGrupo();
        $data['marcas']=$this->insumosModel->getMarca();
		$data['vista'] = 'insumos/crear';
		$this->load->view('index', $data);
	}

	function listado(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['table'] = true;
        $data['insumos']= $this->insumosModel->getInsumos();
        $data['grupos']= $this->insumosModel->getGrupo();
        $data['marcas']= $this->insumosModel->getMarca();
		$data['vista'] = 'insumos/listado';
		$this->load->view('index', $data);
	}
    
    function addInsumo(){
        $fecha = getdate();
        $clave = strtoupper(substr($this->input->post('nombre'),0,1)).strtoupper(substr($this->input->post('marca_id'),0,1)).strtoupper(substr($this->input->post('modelo'),0,1)).$fecha['year'];
        $insumo = array(
        'clave'=>$clave,
        'nombre'=>$this->input->post('nombre'),
        'descripcion'=>$this->input->post('descripcion'),
            'grupo_id'=>$this->input->post('grupo_id'),
            'marca_id'=>$this->input->post('marca_id'),
            'modelo'=>$this->input->post('modelo'),
            'precio'=>$this->input->post('precio')
        );
        if($this->insumosModel->addInsumo($insumo)){echo json_encode(array('res'=>'ok','id_insumo'=>$this->db->insert_id()));}else{echo json_encode(array('res'=>'error'));}
    }

    function addImgInsumo(){
        $id_insumo = $_GET['insumo'];
        if(isset($_FILES['img'])){
            $img = $_FILES['img'];
            $nombreImg = $img['name'];
            $url = 'assets/img/uploads/products/'.$nombreImg;
            move_uploaded_file($img['tmp_name'], './assets/img/uploads/products/'.$nombreImg);
            $this->db->set('img', $url);
            $this->db->where('id', $id_insumo);
            $this->db->update('insumos');
        }
    }

    function editImgInsumo(){
             $id_insumo = $_GET['insumo'];//Recibimos el id del insumo a editar, lo mandamos por get en la url en el ajax
             $insumo = $this->db->get_where('insumos', array('id'=>$id_insumo))->row();//Obtenemos el insumo a editar
             $oldImg = $insumo->img;//Guardamos el campo de la imagen del insumo en una variable
             unlink('./'.$oldImg);//Borramos del servidor la imagen vieja
             $newImg = $_FILES['imgEdit'];//Guardamos la info de la nueva imagen en una variable
             $nombreImg = $newImg['name'];//Guardamos el nombre de la nueva imagen en una variable
             $newUrl = 'assets/img/uploads/products/'.$newImg['name'];//Creamos la url de la nueva imagen
             move_uploaded_file($newImg['tmp_name'], './assets/img/uploads/products/'.$nombreImg);//Movemos la imagen al servidor
             $this->db->set('img', $newUrl);
             $this->db->where('id', $id_insumo);
             $this->db->update('insumos');
             //echo "ok";
    } 

    function getInsumo(){
        $id_insumo= $this->input->post('id_insumo');
        $this->db->where('id',$id_insumo);
        echo json_encode($this->db->get('insumos')->row());
    }

    function editInsumo(){
         $id_insumo = $this->input->post('id_insumo');
            $insumoData = array(
            'nombre'=>$this->input->post('nombre'),
            'descripcion'=>$this->input->post('descripcion'),
            'grupo_id'=>$this->input->post('grupo_id'),
            'marca_id'=>$this->input->post('marca_id'),
            'modelo'=>$this->input->post('modelo'),
            'precio'=>$this->input->post('precio')
        );
        if($this->insumosModel->editInsumo($id_insumo,$insumoData)){echo json_encode(array('res'=>'ok', 'imagen'=>'Llego imagen al editar'));}else{ echo json_encode(array('res'=>'error'));}    
    }

    function eliminarInsumo(){
        $id = $this->input->post('id');
      
        $this->db->where('id',$id);
        $this->db->delete('insumos');
        
        $this->db->trans_complete();
        if($this->db->trans_status()=== false)
            echo json_encode(array('res'=>'error'));
        else
            echo json_encode(array('res'=>'ok'));
    }
}
<?php

class Ventas extends CI_Controller{
	function __construct(){
		parent::__construct();
        $this->load->model('ventasModel');
	}

	function crear(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
        $data['grupos'] = $this->ventasModel->getGrupos();
		$data['table'] = true;
        $data['tableAvender'] = true;
		$data['vista'] = 'ventas/crear';
		$this->load->view('index', $data);
	}

	function listado(){
		if($this->session->userdata('sesion')==true){
			if($this->session->userdata('rol')==1){
				$data['ventas'] = true;
				$data['stock'] = true;
				$data['usuarios'] = true;
				$data['insumos'] = true;
				$data['config'] = true;
			}else if($this->session->userdata('rol')==2){
				$data['ventas'] = true;
				$data['stock'] = true;
			}
		}
		$data['table'] = true;
		$data['vista'] = 'ventas/listado';
		$this->load->view('index', $data);
	}
    
    function getProducts(){
        if($productos = $this->ventasModel->getProducts($this->input->post('grupo')))
        echo json_encode(array('productos'=>$productos,'resp'=>'ok'));
        else
        echo json_encode(array('resp'=>'error'));
    }
    
    function validarExistencia(){
        $idproducto = $this->input->post('idproducto');
        $cant = $this->db->query('SELECT cantidad FROM almacen WHERE insumo_id = '.$idproducto)->row();
        if($cant->cantidad == 0){
            echo json_encode(array('message'=>'empty'));
        }else{
            echo json_encode(array('message'=>'exist'));
        }
    }
    
    function getProduct(){ //Utilizar querys
            $idproducto = $this->input->post('idproducto');
            $this->db->where('id', $idproducto);
            $producto = $this->db->get('insumos')->row();
            $this->db->query('UPDATE almacen SET cantidad = cantidad - 1 WHERE insumo_id = '.$idproducto);
            echo json_encode(array('message'=>$producto));
        
    }
    
    function resetStock(){//utilizar querys
        $this->db->set('cantidad', 'cantidad + 1');
        $this->db->where('insumo_id',$this->input->post('idproducto'));
        if($this->db->update('almacen')){
            echo json_encode(array('message'=>true));
        }else{
            echo json_encode(array('message'=>'error'));
        }
    }
}
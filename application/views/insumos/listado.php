<!--Dialog de Jquery UI-->
<div id="dialog" title="Confirmacion de registro" style="display:none">
  <p id="message">
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>
  </p>
</div>
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
    <h3 align="center"><strong>Listado de insumos</strong></h3>
    <table class="table table bordered table-hover" id="tablaUsuarios">
        <thead>
            <tr>
                <th>Clave</th>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Precio</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>    
        </thead>
        <tbody>
           <?foreach($insumos as $insumo){?>
            <tr>
                <td><?= $insumo->clave?></td>
                <td><?= $insumo->nombre?></td>
                <td><?= $insumo->nom?></td>
                <td><?= $insumo->precio?></td>
                <td><span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="getInsumo(<?=$insumo->id?>)"></span></td>
                <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteInsumo(<?= $insumo->id?>)"></span></td>
            </tr>
            
            <?}?>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Producto</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6">
            <h3 align="center">Imagen del producto</h3>
            <div class="col-lg-12">
                <div class="row" id="preview">
                  <img id="editImgInsumo" width="100%" src="" onerror="this.src='<?=base_url('assets/img/default.jpg')?>';">
                </div>
                <hr>
                <form id="editImgForm" enctype="multipart/form-data" method="POST">
                  <input type="file" id="editImgInput" name="imgEdit" class="form-control">
                </form>
            </div>
          </div>
          <div class="col-lg-6">
            <h3 align="center">Informacion del producto</h3>
            <div class="col-lg-12" style="display:none">
                <input type="text" id="id_insumo">
              </div>
            <div class="col-lg-12">
                <label>Nombre</label>
                <input type="text" class="form-control"  placeholder="Nombre del insumo" id="nombre">
              </div>
              <div class="col-lg-12">
                <label>Descripcion</label>
                <textarea class="form-control" id="descripcion" placeholder="Descripcion" style="resize:none;"></textarea>
              </div>
              <div class="col-lg-12">
                <label>Categoria</label>
                <select class="form-control" name="" id="grupo_id" >
                  <option selected="selected">Escoja una categoria</option>
                 <?foreach ($grupos as $grupo){?>
                  <option value="<?=$grupo->id?>"><?= $grupo->nombre?> </option>
                  <?}?>
                </select>
              </div>
              <div class="col-lg-12">
                <label>Marca</label>
                <select class="form-control"  id="marca_id">
                  <option selected="selected">Escoja una marca</option>
                  <?foreach($marcas as $marca){?>
                  <option  value="<?= $marca->id?>"><?=$marca->nombre?> </option>
                 <?}?>
                </select>
              </div>
              <div class="col-lg-12">
                <label>Modelo</label>
                <input type="text" class="form-control"  placeholder="Modelo" id="modelo">
              </div>
              <div class="col-lg-12">
                  <label>Precio</label>
                  <input type="number" class="form-control"  placeholder="$ 00.00" id="precio">
              </div>
          </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="editInsumo()">Guardar</button>
      </div>
    </div>
  </div>
</div>
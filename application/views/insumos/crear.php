<? require("dialog.php"); ?>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated fadeInRight">
   <h3 align="center"><strong>Captura de producto</strong></h3>
  <div class="col-lg-6">
           <form id="myform" enctype="multipart/form-data">
            <div class="row">
              <div class="form-group col-lg-12">
                <label for=""> Nombre del producto</label>
               <input class="form-control" type="text" name="" placeholder="Nombre" id="nombre">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-lg-12">
                <label for="">Descripcion del producto</label>
                <textarea class="form-control" rows="5"  style="resize:none;" name="" id="descripcion"></textarea>  
              </div>
            </div>
            <div class="row">
              <div class="form-group col-lg-12">
                <label for="">Categoria del producto</label> 
                <select class="form-control" name="" id="grupo_id" >
                  <option selected="selected">Escoja una categoria</option>
                  <? foreach($grupo as $categoria){?>
                  <option value="<?= $categoria->id?>"><?= $categoria->nombre?></option>
                  <?}?> 
                </select>
              </div>
            </div>
             <div class="row">
              <div class="form-group col-lg-12">
                <label for="">Marca del producto</label> 
                <select class="form-control"  id="marca_id">
                  <option selected="selected">Escoja una marca</option>
                  <? foreach($marcas as $marca){?>
                  <option  value="<?= $marca->id?>"><?= $marca->nombre?></option>
                  <?}?>
                </select>
              </div>
            </div> 
            <div class="row">
              <div class="form-group col-lg-12">
                <label for=""> Modelo del producto</label> 
                <input class="form-control" type="text" name="" placeholder="Modelo" id="modelo">  
              </div>
            </div>  
            <div class="row">
              <div class="form-group col-lg-12">
                <label for=""> Precio del producto</label> 
                <input class="form-control" type="number" name="" placeholder="$ 0.00" id="precio">  
              </div>
            </div>
            <div class="row">
              <div class="form-group col-lg-3">
                <button type="button" class="btn btn-success" onclick="addInsumo()">Agregar producto</button>
              </div>
            </div>
           </form>
  </div>
  <div class="col-lg-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 align="center" class="panel-title">Imagen del producto</h3>
      </div>
      <div class="panel-body">
        <div class="row" id="preview">
          <img src="<?=base_url('assets/img/default.jpg')?>" width="100%">
        </div>
        <hr>
        <div class="row">
            <form method="POST" id="imgInsumo" enctype="multipart/form-data">
              <div class="form-group col-lg-12" id="inputFile">
                <input type="file" class="form-control" id="img" name="img">
              </div>
              <div class="form-group col-lg-12" id="input">
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>



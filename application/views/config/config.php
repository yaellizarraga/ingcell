<!--Dialog de Jquery UI-->
<div id="dialog" title="Confirmacion de registro" style="display:none">
  <p id="message">
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>
  </p>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
	<ul class="nav nav-tabs">
	  <li role="presentation" <?=(isset($_GET['active1']))?'class="active"':''?>><a data-toggle="tab" href="#grupos">Grupos</a></li>
	  <li role="presentation" <?=(isset($_GET['active2']))?'class="active"':''?>><a data-toggle="tab" href="#marcas">Marcas</a></li>
	  <!--<li role="presentation" <?//=(isset($_GET['active3']))?'class="active"':''?>><a data-toggle="tab" href="#sucursales">Sucursales</a></li>-->
     <!--Hasta confirmar la implementacion de sucursales se implementara esta pestaña comentada arriba-->
   </ul>

   <div class="tab-content">
   		 <div id="grupos" <?=(isset($_GET['active1']))?'class="tab-pane in active"':'class="tab-pane"'?>>
   		 	<h3 align="center">Gestion de grupos de insumos</h3>
   		 	 <div class="row">
   		 	 	<div class="col-lg-6">
   		 	 		<form>
                     <input type="text" style="display:none;" id="idgrupo">
   		 	 		    <div class="form-group col-lg-12">
   		 	 		    	<label for="group">Nombre del grupo:</label>
   		 	 		    	<input type="text" id="grupo" class="form-control">
   		 	 			</div>
   		 	 			<div class="form-group col-lg-12" id="botonGrupo">
   		 	 				<button type="button" class="btn btn-primary" onclick="saveGroup()">Guardar</button>
   		 	 			</div>
   		 	 		</form>
   		 	 	</div>
   		 	 	<div class="col-lg-6">
   		 	 		<table id="tablaUsuarios" class="table table bordered table-hover">
   		 	 			<thead>
   		 	 				<th>Nombre</th>
   		 	 				<th>Estado</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
   		 	 			</thead>
   		 	 			<tbody id="groups">
   		 	 				<?foreach($grupos as $grupo){?>
   		 	 				 <tr>
   		 	 				 	<td><?=$grupo->nombre?></td>
   		 	 				 	<td><?=($grupo->status==1)?'<span style="color:green;"><strong>Activo</strong></span>':'<span style="color:red;"><strong>Inactivo</strong></span>'?></td>
   		 	 				   <td><span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="getElement(<?=$grupo->id?>,'grupo')"></span></td>
                           <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteElement(<?=$grupo->id?>, 1)"></span></td>
                         </tr>
   		 	 				<?}?>
   		 	 			</tbody>
   		 	 		</table>
   		 	 	</div>
   		 	 </div>
   		 </div>
   		 <div id="marcas" <?=(isset($_GET['active2']))?'class="tab-pane in active"':'class="tab-pane"'?>>
   		 	<h3 align="center">Gestion de marcas de insumos</h3>
   		 	 <div class="row">
   		 	 	<div class="col-lg-6">
   		 	 		<form>
                     <input type="text" style="display:none;" id="idmarca">
   		 	 		    <div class="form-group col-lg-12">
   		 	 		    	<label for="marca">Nombre de la marca:</label>
   		 	 		    	<input type="text" id="marca" id="marca" class="form-control">
   		 	 			</div>
   		 	 			<div class="form-group col-lg-12" id="botonMarca">
   		 	 				<button type="button" class="btn btn-primary" onclick="saveMarca()">Guardar</button>
   		 	 			</div>
   		 	 		</form>
   		 	 	</div>
   		 	 	<div class="col-lg-6">
   		 	 		<table id="tablaUsuarios2" class="table table bordered table-hover">
   		 	 			<thead>
   		 	 				<th>Nombre</th>
   		 	 				<th>Estado</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
   		 	 			</thead>
   		 	 			<tbody id="groups">
   		 	 				<?foreach($marcas as $marca){?>
   		 	 				 <tr>
   		 	 				 	<td><?=$marca->nombre?></td>
   		 	 				 	<td><?=($marca->status==1)?'<span style="color:green;"><strong>Activa</strong></span>':'<span style="color:red;"><strong>Inactiva</strong></span>'?></td>
                           <td><span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="getElement(<?=$marca->id?>,'marcas')"></span></td>
                           <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteElement(<?=$marca->id?>, 2)"></span></td>
                         </tr>
   		 	 				<?}?>
   		 	 			</tbody>
   		 	 		</table>
   		 	 	</div>
   		 	 </div>
   		 </div>
   		 <div id="sucursales" <?=(isset($_GET['active3']))?'class="tab-pane in active"':'class="tab-pane"'?>>
   		 	<h3 align="center">Gestion de sucursales</h3>
   		 	 <div class="row">
   		 	 	<div class="col-lg-6">
   		 	 		<form>
   		 	 		    <div class="form-group col-lg-12">
   		 	 		    	<label for="suc">Nombre de la sucursal:</label>
   		 	 		    	<input type="text" id="suc" class="form-control">
   		 	 			</div>
                      <div class="form-group col-lg-12">
                        <label for="direccion">Direccion:</label>
                        <input type="text" id="direccion" class="form-control">
                     </div>
                      <div class="form-group col-lg-12">
                        <label for="telefono">Telefono:</label>
                        <input type="text" id="telefono" class="form-control">
                     </div>
   		 	 			<div class="form-group col-lg-12">
   		 	 				<button type="button" class="btn btn-primary" onclick="saveSucursal()">Guardar</button>
   		 	 			</div>
   		 	 		</form>
   		 	 	</div>
   		 	 	<div class="col-lg-6">
   		 	 		<table id="tablaUsuarios3" class="table table bordered table-hover">
   		 	 			<thead>
   		 	 				<th>Nombre</th>
   		 	 				<th>Direccion</th>
   		 	 				<th>Telefono</th>
   		 	 				<th>Estado</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
   		 	 			</thead>
   		 	 			<tbody id="groups">
   		 	 				<?foreach($sucursales as $sucursal){?>
   		 	 				 <tr>
   		 	 				 	<td><?=$sucursal->nombre?></td>
   		 	 				 	<td><?=$sucursal->direccion?></td>
   		 	 				 	<td><?=$sucursal->telefono?></td>
   		 	 				 	<td><?=($sucursal->status==1)?'<span style="color:green;"><strong>Activo</strong></span>':'<span style="color:red;"><strong>Baja</strong></span>'?></td>
                           <td><span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="getSucursal()"></span></td>
                           <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick=""></span></td>
                         </tr>
   		 	 				<?}?>
   		 	 			</tbody>
   		 	 		</table>
   		 	 	</div>
   		 	 </div>
   		 </div>
   </div>
</div>
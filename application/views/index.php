<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sistema ING-CELL</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Jquery UI -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/jqueryui/jquery-ui.min.css')?>">
    <!-- Jquery dataTable -->
    <link rel="stylesheet" href="<?= base_url('/assets/css/plugins/jqueryUi/dataTables.bootstrap.min.css')?>">
    <!-- Custom CSS -->
    <link href="<?=base_url('assets/css/sb-admin.css')?>" rel="stylesheet">
    <?if(isset($home)){?>
    <!-- Morris Charts CSS -->
    <link href="<?=base_url('assets/css/plugins/morris.css')?>" rel="stylesheet">
    <?}?>
    <!-- Custom Fonts -->
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/animate.css')?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Ing Cell Administrador</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=$this->session->userdata('user')?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=base_url('almacen/logout')?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?=base_url('almacen/home')?>"><i class="fa fa-fw fa-dashboard"></i> Administrador</a>
                    </li>
                     <?if(isset($usuarios)){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-user"></i> Usuarios<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="<?=base_url('usuarios/crear')?>">Crear usuario</a>
                            </li>
                            <li>
                                <a href="<?=base_url('usuarios/listado')?>">Listado de usuarios</a>
                            </li>
                            <li>
                                <a href="<?=base_url('usuarios/empleado')?>">Crear empleado</a>
                            </li>
                            <li>
                                <a href="<?=base_url('usuarios/empleado_listado')?>">Listado de empleados</a>
                            </li>
                        </ul>
                    </li>
                    <?}?>
                    <?if(isset($insumos)){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#insumos"><i class="fa fa-fw fa-tags"></i> Productos<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="insumos" class="collapse">
                            <li>
                                <a href="<?=base_url('insumos/crear')?>">Crear producto</a>
                            </li>
                            <li>
                                <a href="<?=base_url('insumos/listado')?>">Listado de productos</a>
                            </li>
                        </ul>
                    </li>
                    <?}?>
                    <?if(isset($ventas)){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ventas"><i class="fa fa-fw fa-shopping-cart"></i> Ventas<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="ventas" class="collapse">
                            <li>
                                <a href="<?=base_url('ventas/crear')?>">Realizar venta</a>
                            </li>
                            <li>
                                <a href="<?=base_url('ventas/listado')?>">Listado de ventas</a>
                            </li>
                        </ul>
                    </li>
                    <?}?>
                    <?if(isset($stock)){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#stock"><i class="fa fa-fw fa-sort"></i> Stock<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="stock" class="collapse">
                            <li>
                            <a href="<?=base_url('almacen/stock')?>">Ver stock</a>
                            </li>
                        </ul>
                    </li>
                    <?}?>
                     <?if(isset($config)){?>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#config"><i class="fa fa-fw fa-wrench"></i> Configuracion<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="config" class="collapse">
                            <li>
                              <a href="<?=base_url('config/gestionar?active1=true')?>">Grupos / Sucursales / Marcas</a>
                            </li>
                        </ul>
                    </li>
                    <?}?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
                <?include($vista.'.php');?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?=base_url('assets/js/jquery.js')?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
       <!-- Bootstrap Core JavaScript for jquery tables -->
    <script src="<?=base_url('assets/jqueryui/jquery-ui.min.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/jqueryUi/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/jqueryUi/dataTables.bootstrap.min.js')?>"></script>
    <script src="<?=base_url('assets/js/app.js')?>"></script>
    <?if(isset($home)){?>
    <!-- Morris Charts JavaScript -->
    <script src="<?=base_url('assets/js/plugins/morris/raphael.min.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/morris/morris.min.js')?>"></script>
    <script src="<?=base_url('assets/js/plugins/morris/morris-data.js')?>"></script>
    <script type="text/javascript">
        // Bar Chart
        Morris.Bar({
            element: 'morris-bar-chart',
            data: [
              <?$cont=0;foreach($barChart as $chart){$cont=$cont+1;?>
              {
                Producto: "<?=$chart->producto?>",
                vendidos: <?=$chart->vendidos?>
            }<?=($cont==6)?'':','?>
            <?}?>
          ],
            xkey: 'Producto',
            ykeys: ['vendidos'],
            labels: ['Vendidos'],
            barRatio: 0.4,
            xLabelAngle: 35,
            hideHover: 'auto',
            resize: true
        });

        // Donut Chart
        Morris.Donut({
            element: 'morris-donut-chart',
            data: [
            <?$cont = 0;foreach($donutChart as $chart){ $cont=$cont+1;?>
            {
                label: "<?=$chart->producto?>",
                value: <?=$chart->cantidad?>
            }<?=($cont==5)?'':','?>
            <?}?>
          ],
            resize: true
        });
    </script>
    <?}?>
    <!--Creacion de las tablas con filtros y paginados-->
    <?if(isset($table)){?>
        <script type="text/javascript">
          $(document).ready(function(){
            $('#tablaUsuariosVentas').dataTable({searching: false,paging:false,info:false});
          });
        </script>
    <?}?>
    <?if(isset($table2)){?>
        <script type="text/javascript">
          $(document).ready(function(){
            $('#tablaUsuarios2').dataTable();
          });
        </script>
    <?}?>
    <?if(isset($table3)){?>
        <script type="text/javascript">
          $(document).ready(function(){
            $('#tablaUsuarios3').dataTable();
          });
        </script>
    <?}?>
    <?if(isset($tableAvender)){?>
        <script type="text/javascript">
          $(document).ready(function(){
            $('#tablaUsuarios4').dataTable({searching: false,paging:false,info:false});
          });
        </script>
    <?}?>
</body>
</html>

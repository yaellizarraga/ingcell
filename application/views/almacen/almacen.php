<? require("dialog.php"); ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
    <h3 align="center">
        <strong>Stock actual del negocio</strong>
    </h3>
	<table class="table table bordered table-hover" id="tablaUsuarios">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Agregar cantidad</th>
                <th>Cantidad existente </th>
            </tr>    
        </thead>
        <tbody>
            <tr>
                <td>0001</td>
                <td>protector de pantalla</td>
                <td><a href="#" class="btn btn-default">Ver descripcion</a></td>
                <td><a href="#" class="btn btn-default">Agregar cantidad</a></td>
                <td>60</td>             
            </tr>
            <tr>
                <td>0002</td>
                <td>protector de pantalla</td>
                <td><a href="#" class="btn btn-default">Ver descripcion</a></td>
                <td><a href="#" class="btn btn-default">Agregar cantidad</a></td>

                <td>65</td>
            </tr>
            <tr>
                <td>0003</td>
                <td>tarjeta madre</td>
                <td><a href="#" class="btn btn-default">Ver descripcion</a></td>
                <td><a href="#" class="btn btn-default">Agregar cantidad</a></td>

                <td>560</td>
            </tr>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Test modal</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
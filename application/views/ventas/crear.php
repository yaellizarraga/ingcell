<?php require("dialog.php"); ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
	<h3 align="center"><strong>Busqueda de productos</strong></h3>
	<div class="row" id="filters">
		<div class="col-lg-4">
			<label for="insumoCate">Categorias de productos</label> 
			<select class="form-control" id="insumoCate" name="insumoCate">
			<option value="0" selected="selected">Seleccione la categoria</option>
                <?php foreach($grupos as $grupo){?>
                 <option value="<?=$grupo->id?>"><?=$grupo->nombre?></option>
                <?php } ?>
		</select>
		</div>
		<div class="col-lg-4" style="display:none">
			<label for="code">Codigo de producto</label> 
			<input class="form-control" type="text" name="code" id="code" placeholder="Codigo del insumo">
		</div>
		<div class="col-lg-4" style="display:none">
			<label for="name">Nombre de producto</label> 
			<input class="form-control" type="text" name="name" id="name" placeholder="Nombre del insumo">
		</div>
	</div>
	<hr>
	<h3 align="center"><strong>Productos</strong></h3>
	<div class="row" id="tableInsumos">
		<div class="col-lg-12">
			<table class="table" id="tablaUsuariosVentas">
			<thead>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Marca</th>
				<th>Precio</th>
				<th>Accion</th>
			</thead>
			<tbody>
				<!--Aqui se pondran los productos desde la base de datos con ajax-->
			</tbody>
		</table>
		</div>
	</div>
	<hr>
	<h3 align="center"><strong>Productos a vender</strong></h3>
	<div class="row" id="shopingCart">
		<div class="col-lg-12">
			<table class="table" id="tablaUsuarios4">
			<thead>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Marca</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>Accion</th>
			</thead>
			<tbody>
				<!--Aqui se pondran los productos desde la base de datos con ajax-->
			</tbody>
		</table>
		</div>
	</div>
	<hr>
	<div class="row" id="shop">
			<div class="row">
				<div class="form-group col-lg-3">
					<label for="iva">iva</label>
					<input class="form-control" type="text" name="iva" id="iva" placeholder="IVA">
				</div>
				<div class="form-group col-lg-3">
					<label for="subtotal">Subtotal</label>
					<input class="form-control" type="text" name="subtotal" id="subtotal" placeholder="Subtotal">
				</div>
				<div class="form-group col-lg-3">
					<label for="total">Total</label>
					<input class="form-control" type="text" name="total" id="total" placeholder="Total">
				</div>
				<div class="form-group col-lg-3">
					<label for="total">Agregar Descuento</label>
					<input class="form-control" type="text" name="descuento" id="descuento" placeholder="00.00 %">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-4">
					<button type="button" class="btn btn-primary">Confirmar venta</button>
				</div>
				<div class="form-group col-lg-4">
				</div>
				<div class="form-group col-lg-4">
				</div>
			</div>
	</div>
</div>
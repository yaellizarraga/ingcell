<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
	<h3 align="center"><strong>Listado de ventas</strong></h3>

    <table class="table table bordered table-hover" id="tablaUsuarios">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>iva</th>
                <th>Sub. Total</th> 
                <th>Total</th>	
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>    
        </thead>
        <tbody>
            <tr>
                <td>19/04/2017</td>
                <td>$15.50</td>
                <td>$405.00</td>
                <td>$420.50</td>
                <td><span class="glyphicon  glyphicon-refresh" aria-hidden="true" onclick="getVenta(1)"></span></td>
                <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteVenta(1)"></span></td>
               
                
            </tr>
            <tr>
                <td>19/04/2017</td>
                <td>$15.50</td>
                <td>$405.00</td>
                <td>$420.50</td>
                <td><span class="glyphicon  glyphicon-refresh" aria-hidden="true"></span></td>
                <td><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                
            </tr>
            <tr>
                <td>19/04/2017</td>
                <td>$15.50</td>
                <td>$405.00</td>
                <td>$420.50</td>
                <td><span class="glyphicon  glyphicon-refresh" aria-hidden="true"></span></td>
                <td><span class="glyphicon   glyphicon-trash" aria-hidden="true"></span></td>
                
            </tr>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Test modal</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!--Dialog de Jquery UI-->
<div id="dialog" title="Confirmacion de operacion" style="display:none">
  <p id="message">
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin: 0 7px 50px 0;"></span>
  </p>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
    <h3 align="center"><strong>Listado de usuarios</strong></h3>
    <table class="table table bordered table-hover" id="tablaUsuarios">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Rol</th>
                <th>Estado</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>    
        </thead>
        <tbody>
            <?foreach($usuarios as $usuario){?>
            <tr>
                <td><?=$usuario->usuario?></td>
                <td><?=($usuario->rol==1)?'Administrador':'Ventas'?></td>
                <td><?=($usuario->estado==1)?'<span style="color:green;"><strong>Activo</strong></span>':'<span style="color:red;"><strong>Baja</strong></span>'?></td>
                <td><span class="glyphicon  glyphicon-refresh" aria-hidden="true" onclick="getUser(<?=$usuario->id?>)"></span></td>
                <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteUser(<?=$usuario->id?>)"></span></td> 
            </tr>
            <?}?>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Test modal</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <input style="display:none;" id="iduser" type="text">
          <div class="form-group col-lg-12">
          <label> Seleccione el nombre del empleado</label>
                    <select class="form-control" id="empleados">
                        <option value ="0" selected="selected">Escoja un empleado</option>
                        <?foreach($empleados as $empleado){?>
                        <option value="<?=$empleado->id?>"><?=$empleado->nombre." ".$empleado->apaterno?></option>
                        <?}?>
                    </select>
          </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="user">Usuario</label>
          <input type="text" id="user" class="form-control" placeholder="Ingrese el usuario">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="pass">Ingrese la contraseña</label>
              <input type="password" class="form-control" placeholder="Ingrese la contraseña" id="pass">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="rol">Seleccione el tipo de permiso</label>
          <select id="rol" class="form-control">
            <option value="0" selected="selected">Escoja el rol del usuario</option>
            <option value="1">Administrador</option>
            <option value="2">Ventas</option>
          </select>
        </div>
      </div>
       <div class="row">
        <div class="form-group col-lg-12">
          <label for="estado">Seleccione el estado</label>
          <select id="estado" class="form-control">
            <option value="3" selected="selected">Escoja el estado del usuario</option>
            <option value="1">Activo</option>
            <option value="0">Baja</option>
          </select>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="editUsuario()">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>
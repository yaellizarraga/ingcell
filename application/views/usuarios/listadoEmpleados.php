<!--Dialog de Jquery UI-->
<div id="dialog" title="Confirmacion de operacion" style="display:none">
  <p id="message"></p>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
    <h3 align="center"><strong>Listado de empleados</strong></h3>
    <table class="table table bordered table-hover" id="tablaUsuarios">
        <thead>
            <tr>
                <th>Clave</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Numero de telefono</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>    
        </thead>
        <tbody>
            <?foreach($empleados as $empleado){?>
            <tr>
                <td><?=$empleado->clave?></td>
                <td><?=$empleado->nombre?></td>
                <td><?=$empleado->apaterno?></td>
                <td><?=$empleado->telefono?></td>
                <td><span class="glyphicon glyphicon-refresh" aria-hidden="true" onclick="getEmpleado(<?=$empleado->id?>)"></span></td>
                <td><span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="deleteEmpleado(<?=$empleado->id?>)"></span></td>
            </tr>
            <?}?>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edicion de Empleado</h4>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-lg-4" style="display:none">
                <input type="text" id="idemp">
              </div>
              <div class="col-lg-4">
                <label>Nombre</label>
                <input type="text" class="form-control"  placeholder="Nombre del empleado" id="nombre">
              </div>
              <div class="col-lg-4">
                <label>Apellido Paterno</label>
                <input type="text" class="form-control"  placeholder="Apellido Paterno" id="apa">
              </div>
              <div class="col-lg-4">
                <label>Apellido Materno</label>
                <input type="text" class="form-control"  placeholder="Apellido Materno" id="ama">
              </div>
              <div class="col-lg-4">
                <label>Direccion</label>
                <input type="text" class="form-control"  placeholder="Direccion" id="dir">
              </div>
              <div class="col-lg-4">
                <label>Telefono/Celular</label>
                <input type="text" class="form-control"  placeholder="669-000-00-00" id="tel">
              </div>
              <div class="col-lg-4">
                  <label>Correo Electronico</label>
                  <input type="email" class="form-control"  placeholder="empleado@empleado.com" id="mail">
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="editEmpleado()">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>
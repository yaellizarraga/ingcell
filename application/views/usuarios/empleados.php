<? require('dialog.php');?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInRight">
	<h3 align="center"><strong>Registro de empleados</strong></h3>
  <div class="col-lg-6">
    <form method="" action="" name="">
      <div class="row">
           <div class="form-group col-lg-12">
            <label>Nombre</label>
            <input type="text" class="form-control obligatorio"  placeholder="Nombre del empleado" id="nombre">
          </div> 
      </div>
          <div class="row">
           <div class="form-group col-lg-12">
            <label>Apellido Paterno</label>
            <input type="text" class="form-control obligatorio"  placeholder="Apellido Paterno" id="apa">
          </div> 
      </div>
      <div class="row">
           <div class="form-group col-lg-12">
            <label>Apellido Materno</label>
            <input type="text" class="form-control obligatorio"  placeholder="Apellido Materno" id="ama">
          </div> 
      </div>
      <div class="row">
           <div class="form-group col-lg-12">
            <label>Direccion</label>
            <input type="text" class="form-control obligatorio"  placeholder="Direccion" id="dir">
          </div> 
      </div>
      <div class="row">
           <div class="form-group col-lg-12">
            <label>Numero de Telefono/Celular</label>
            <input type="text" class="form-control obligatorio"  placeholder="669-000-00-00" id="tel">
          </div> 
      </div>
      <div class="row">
           <div class="form-group col-lg-12">
            <label>Correo Electronico</label>
            <input type="email" class="form-control obligatorio"  placeholder="empleado@empleado.com" id="mail">
          </div> 
      </div> 
          <div class="row" style=" display:none;">
          <label>Sucursal</label>
           <select class="form-control col-lg-12" id="suc">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
      </div>
           <button type="button" class="btn btn-success" onclick="addEmpleados()">Agregar empleado</button>
        </form>
  </div>
</div>
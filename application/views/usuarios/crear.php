<? require("dialog.php"); ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated fadeInRight">
  <h3 align="center"><strong>Creacion de cuentas de usuario</strong></h3>
  <div class="col-lg-6">
    <form method="" action="" name="">
      <div class="row">
        <div class="form-group col-lg-12">
        <label> Seleccione el nombre del empleado</label>
                  <select class="form-control" id="empleados">
                      <option value ="0" selected="selected">Escoja un empleado</option>
                      <?foreach($empleados as $empleado){?>
                      <option value="<?=$empleado->id?>"><?=$empleado->nombre." ".$empleado->apaterno?></option>
                      <?}?>
                  </select>
      </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="user">Usuario</label>
          <input type="text" id="user" class="form-control" placeholder="Ingrese el usuario">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="pass">Ingrese la contraseña</label>
              <input type="password" class="form-control" placeholder="Ingrese la contraseña" id="pass">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-lg-12">
          <label for="rol">Seleccione el tipo de permiso</label>
          <select id="rol" class="form-control">
            <option value="0" selected="selected">Escoja el rol del usuario</option>
            <option value="1">Administrador</option>
            <option value="2">Ventas</option>
          </select>
        </div>
      </div>
           <button type="button" class="btn btn-success" onclick="addUser()">Agregar usuario</button>
        </form>
  </div>
  <div class="col-lg-6">  
        <div class="panel panel-primary">
           <div class="panel-heading">
                <h3 align="center" class="panel-title">Informacion sobre los tipos de usuarios</h3>
           </div>
           <div class="panel-body">
                <label>Administrador</label>
                <p>Acceso a todos los modulos del administrador</p>
                <hr>
                <label>Vendedor</label> 
                <p>Acceso a los modulos de <strong>VENTAS</strong> y <strong>STOCK</strong></p>
           </div>
          </div>    
  </div>
</div>

        

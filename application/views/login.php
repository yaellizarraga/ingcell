<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login de acceso | INGCELL</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap Core CSS -->
    <link type="text/css" href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/animate.css')?>">
    <style type="text/css">
      body{
        background-color: #212121;
        color: white;
      }

      /* Sticky footer styles
-------------------------------------------------- */
      html {
        position: relative;
        min-height: 100%;
      }
      .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        /* Set the fixed height of the footer here */
        height: 60px;
        background-color:#424242;
      }

      .footer .container .text-muted {
        margin: 20px 0;
        color: white;
      }

    </style>
</head>
<body>
	<script type="text/javascript">
    var base_url = 'http://localhost/ingcell/';
    function validateLogin(event){
          event.preventDefault();
          if($('#txtuser').val()=='' || $('#txtpassword').val()==''){
              $('#login-message-container').addClass("alert alert-danger");
              $('#login-message').html("Ingrese los datos");
          }else{
            $.ajax({
              type:'POST',
              url:base_url+'almacen/login',
              data:{"user":$('#txtuser').val(),"password":$('#txtpassword').val()},
              success:function(response){
                console.log(response.res);
                if(response.res!="00"){
                    window.location.href = base_url+'almacen/home';
                }else {
                 	  $('#login-message-container').addClass("alert alert-danger");
                    $('#login-message').html("Usuario y/o Contraseña inexistentes!");
                 }
              },
              error:function(){
              $('#login-message-container').addClass("alert alert-danger");
              $('#login-message').html("Ha ocurrido un error");},
              dataType:'json'
            });
          }

    }
    </script>
	<div class="container">
      <div id="page-wrapper" style="padding-top:10%">
          <div class="container-fluid animated fadeInRight" style="padding-bottom:10%;">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
              <!--<img src="<?//=base_url('assets/img/logo-zn2.png')?>" alt="logo" style="width:100%">-->
              <h3 align="center">Login de acceso</h3>
                <form id="loginForm">
                    <div class="row">
                      <div class="form-group">
                        <label for="txtuser">Usuario</label>
                        <input type="text" name="txtuser" id="txtuser" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <label for="txtuser">Contraseña</label>
                        <input type="password" name="txtpassword" id="txtpassword" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                          <button type="submit" onclick="validateLogin(event)" class="btn btn-success btn-lg btn-block">Entrar</button>
                        </div>
                    </div>
                    <div class="row">
                      <div role="alert" id="login-message-container"><span id="login-message"></span></div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4"></div>
          </div>
      </div>
    </div>
     <footer class="footer">
      <div class="container">
        <p class="text-muted">
          <small>Todos los derechos reservados, INGCELL &copy;</small>
        </p>
      </div>
    </footer>
	<!-- jQuery -->
    <script src="<?=base_url('assets/js/jquery.js')?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
</body>
</html>
